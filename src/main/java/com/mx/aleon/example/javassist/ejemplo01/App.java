package com.mx.aleon.example.javassist.ejemplo01;

import java.io.IOException;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;

public class App {
	public static void main(String[] args) throws NotFoundException, IOException, CannotCompileException {
		ClassPool pool = ClassPool.getDefault();
		CtClass cc = pool.get("test.Rectangulo");
		byte[] b = cc.toBytecode();
		System.out.println("Bytes de clase rectangulo antes de agregar a punto: "+ b.length);
		cc.defrost();

		cc = pool.get("test.Rectangulo");
		cc.setSuperclass(pool.get("test.Punto"));
		cc.writeFile();

		b = cc.toBytecode();
		System.out.println("Bytes de clase rectangulo despues de agregar a punto: " + b.length);
		cc.defrost();
	}
}
